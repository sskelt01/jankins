module bitbucket.org/sskelt01/jankins

go 1.12

require (
	github.com/Bowery/prompt v0.0.0-20190419144237-972d0ceb96f5 // indirect
	github.com/labstack/gommon v0.2.8 // indirect
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/mkideal/cli v0.0.2
	github.com/mkideal/pkg v0.0.0-20170503154153-3e188c9e7ecc // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/yosida95/golang-jenkins v0.0.0-20190404125845-4772716c47ca
)
