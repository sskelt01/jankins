package jenkins

import (
	"fmt"
	"net/url"
	"os"

	"github.com/satori/go.uuid"
	"github.com/yosida95/golang-jenkins"
)

var (
	runLabel    = "runName"
	sourceLabel = "sourceBranch"
)

//GetJenkinsCreds asas
func GetJenkinsCreds() *gojenkins.Jenkins {
	// fmt.Printf("%s lives in %s.\n", os.Getenv("USER"), os.Getenv("HOME"))
	auth := &gojenkins.Auth{
		Username: os.Getenv("JENKINS_USER"),
		ApiToken: os.Getenv("JENKINS_TOKEN"),
	}
	return gojenkins.NewJenkins(auth, os.Getenv("JENKINS_URL"))
}

//TriggerBuild sss
func TriggerBuild(jobName, branch string) error {
	jenkinsClient := GetJenkinsCreds()
	job, err := jenkinsClient.GetJob(jobName)
	if err != nil {
		return err
	}
	uuidv4 := uuid.NewV4()
	uuidBuildID := uuid.Must(uuidv4, err).String()
	v := url.Values{}
	v.Set(runLabel, uuidBuildID)
	if branch != "" {
		v.Set(sourceLabel, branch)
	}
	err = jenkinsClient.Build(job, v)
	if err != nil {
		return err
	}
	fmt.Printf("build uuid: %s\n", uuidBuildID)
	return nil
}

//GetTriggeredBuild aa
func GetTriggeredBuild(jobName, uuidBuildID string) (gojenkins.Build, error) {
	jenkinsClient := GetJenkinsCreds()
	// fmt.Printf("%+v\n", jenkinsClient)
	job, err := jenkinsClient.GetJob(jobName)
	if err != nil {
		return gojenkins.Build{}, err
	}

	// fmt.Printf("%+v\n", job)
	for _, buildStub := range job.Builds {
		build, err := jenkinsClient.GetBuild(job, buildStub.Number)
		if err != nil {
			panic(err)
		}
		for _, action := range build.Actions {
			for _, params := range action.Parameter {
				if params.Name == runLabel && params.Value == uuidBuildID {
					if build.Building {
						// fmt.Printf("%+v\n", build)
						fmt.Println("this is building now, be patient")
						return gojenkins.Build{}, nil
					}
					fmt.Printf("%+v\n", build.Result)
					// fmt.Printf("%+v\n", build)
					return build, nil
				}
			}
		}
	}
	fmt.Println("Couldn't find that ID, it might not have started")
	return gojenkins.Build{}, nil
}
