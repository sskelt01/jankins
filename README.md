Jankins
=======

A litle utility that will kick off a jenkins job from the command line.
The current released versions are in the bin directory, suffixed with
he corss compiled platform name.

There is some convention that will need to be followed to make this work.

Convention:

Jenkins job:
    The Jenkins jobs requires some parameters for this program to work:

        runName: this will hold the the string sent to jenkins for tracking
        sourceBranch: will tell the jenkins job what branch to run

Config:
    There are two places config live, then envirmentment and in a json file within a repo.

    Enviromentment variables:
    * JENKINS_USER: your jenkins username
    * JENKINS_TOKEN: your jenkins api key
    * JENKINS_URL: where the jenkins server is

    JSON file: This tells Jankins what jenkins job to send requests to build
    using the abover config and is contained in the top let of the repo you are working in.
    You can build the json yourself or use the 'init' command to dump the skeleton json,
    an then fill in the required jenkins job. See below an example of a janksins.json file
    {
        "repo": "p5-app-bbcpe--PRS"
    }
