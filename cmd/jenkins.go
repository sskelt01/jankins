package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	"bitbucket.org/sskelt01/jankins/pkg/jenkins"
	"github.com/mkideal/cli"
)

var (
	//Version holds the git has version
	Version = "IN DEV"
	//Build holds the timestamp of the build
	Build = "IN DEV"
)

func main() {

	dir, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	fmt.Println(dir)

	if err := cli.Root(root,
		cli.Tree(help),
		cli.Tree(initaliseConfig),
		cli.Tree(build),
		cli.Tree(buildBranch),
		cli.Tree(getTriggeredBuild),
	).Run(os.Args[1:]); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

var help = cli.HelpCommand("display help information")

// root command
type rootT struct {
	cli.Helper
}

var root = &cli.Command{
	Desc: "this is root command",
	// Argv is a factory function of argument object
	// ctx.Argv() is if Command.Argv == nil or Command.Argv() is nil
	Argv: func() interface{} { return new(rootT) },
	Fn: func(ctx *cli.Context) error {
		ctx.String("Build:   %s\n", Build)
		ctx.String("Version: %s\n", Version)
		ctx.String("Nothing to see here try using help\n")
		return nil
	},
}

type config struct {
	JobName string `json:"job_name"`
}

type rootCli struct {
	cli.Helper
	Force bool `cli:"force" usage:"this is a required flag, note the *"`
}

var initaliseConfig = &cli.Command{
	Name: "init",
	Desc: "inits the config file",
	Argv: func() interface{} { return new(rootCli) },
	Fn: func(ctx *cli.Context) error {
		argv := ctx.Argv().(*rootCli)
		outputFileName := "jankins.json"

		if _, err := os.Stat(outputFileName); !os.IsNotExist(err) {
			if !argv.Force {
				fmt.Printf(`The %s was present and will not be over written,
to force use --force`, outputFileName)
				return nil
			}
		}

		fmt.Printf(`initialising Jankins for this repo.
Please not if this is your first time make sure you have:
JENKINS_USERNAME, JENKINS_APIKEY and JENKINS_URL Set up in your environment.

I will now dump a basic config file to %s, you need to set the correct values to it

`, outputFileName)

		rankingsJson, err := json.Marshal(&config{})
		if err != nil {
			panic(err)
		}
		err = ioutil.WriteFile(outputFileName, rankingsJson, 0644)
		if err != nil {
			panic(err)
		}
		return nil
	},
}

//BaseCli holds the json config and config file name
type BaseCli struct {
	cli.Helper
	JSON config `cli:"c,config" usage:"parse json from file" parser:"jsonfile"`
	File string `cli:"f,file" usage:"uses this file instead of the file within the repo" dft:"jankins.json"`
}

var build = &cli.Command{
	Name: "build",
	Desc: "trigger the default branch for the jenkins jobs",
	Argv: func() interface{} { return new(BaseCli) },
	Fn: func(ctx *cli.Context) error {
		argv := ctx.Argv().(*BaseCli)
		return buildJenkinsTarget(argv.JSON.JobName, "", argv.File)
	},
}

var buildBranch = &cli.Command{
	Name: "buildbranch",
	Desc: "trigger the current checked branch for the jenkins jobs",
	Argv: func() interface{} { return new(BaseCli) },
	Fn: func(ctx *cli.Context) error {
		argv := ctx.Argv().(*BaseCli)

		currentBranch, err := exec.Command("/usr/bin/git", "rev-parse", "--abbrev-ref", "HEAD").Output()
		if err != nil {
			panic(err)
		}
		return buildJenkinsTarget(argv.JSON.JobName, string(currentBranch), argv.File)
	},
}

func buildJenkinsTarget(targetJob, branch, file string) error {
	if targetJob == "" {
		fileConfig := readDefaultConfig(file)
		if fileConfig.JobName == "" {
			panic("No JobName found in the config")
		}
		targetJob = fileConfig.JobName
	}
	if branch == "" {
		fmt.Printf("Going to build the branch: '%s'\n", "THE_DEFAULT")
	} else {
		branch = strings.TrimSuffix(branch, "\n")
		fmt.Printf("Going to build the branch: '%s'\n", branch)
	}
	err := jenkins.TriggerBuild(targetJob, branch)
	if err != nil {
		panic(err)
	}

	return nil
}

type getbuildT struct {
	BaseCli
	ID   string `cli:"*id" usage:"This is the id of the job you want to fetch the status of"`
	Wait bool   `cli:"wait" usage:"Do you want to wait to find out? this will ping the jenkins server every X (default 5) second"`
}

var getTriggeredBuild = &cli.Command{
	Name: "status",
	Desc: "get the status of a build",
	Argv: func() interface{} { return new(getbuildT) },
	Fn: func(ctx *cli.Context) error {
		argv := ctx.Argv().(*getbuildT)

		if argv.JSON.JobName == "" {
			argv.JSON = readDefaultConfig(argv.File)
			if argv.JSON.JobName == "" {
				panic("No Repo found in the config")
			}
		}
		repo := argv.JSON.JobName

		for {
			build, err := jenkins.GetTriggeredBuild(repo, argv.ID)
			if err != nil {
				panic(err)
			}
			if !argv.Wait {
				break
			}
			if build.Result == "SUCCESS" {
				fmt.Println("This build succeeded")
				break
			}
			time.Sleep(5 * time.Second)
		}

		return nil
	},
}

func readDefaultConfig(file string) config {
	conf := &config{}

	dir, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	cli.ReadJSONFromFile(filepath.Join(dir, file), conf)
	return *conf
}
