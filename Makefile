# a super simple make file,
# build will build for your local machine
# build-linux buld for the linux destination


VERSION=`git rev-parse HEAD`
BUILD=`date +%FT%T%z`
LDFLAGS=-ldflags="-X main.Version=${VERSION} -X main.Build=${BUILD} -s -w "
COMPILEFLAGS=-p 2 -v
MAIN="./cmd/jenkins.go"


build:
	go build ${LDFLAGS}  ${COMPILEFLAGS} -o jankins  ${MAIN}

install:
	go build ${LDFLAGS}  ${COMPILEFLAGS} -o jankins  ${MAIN}
	cp jankins /usr/local/bin

build-linux:
	env GOOS="linux" GOARCH="amd64" go build ${LDFLAGS}  ${COMPILEFLAGS} -o bin/jankins-linux ${MAIN}

build-macos:
	env GOOS="darwin" GOARCH="amd64" go build ${LDFLAGS}  ${COMPILEFLAGS} -o bin/jankins-macos ${MAIN}

build-release-files: build-linux build-macos